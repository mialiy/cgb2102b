package com.jt.controller;

import com.jt.pojo.ItemCat;
import com.jt.service.ItemCatService;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/itemCat")
public class ItemCatController {

    @Autowired
    private ItemCatService itemCatService;

    /**
     * 实现商品分类查询
     * URL地址:  /itemCat/findItemCatList
     * 参数: type
     * 返回值: SysResult对象
     */
    @GetMapping("/findItemCatList/{type}")
    public SysResult findItemCatList(@PathVariable Integer type){

        List<ItemCat> itemCatList = itemCatService.findItemCatList(type);
        return SysResult.success(itemCatList);
    }


    /**
     * 实现商品分类状态修改
     * URL: /itemCat/status/{id}/{status}
     * 参数:  id/status
     * 返回值: SysResult对象
     */
    @PutMapping("/status/{id}/{status}")
    public SysResult updateStatus(ItemCat itemCat){

        itemCatService.updateStatus(itemCat);
        return SysResult.success();
    }

    /**
     * 实现商品分类新增
     * URL: /itemCat/saveItemCat
     * 请求参数: 前端采用post/put的方式进行传参 该数据会被解析为JSON
     *          所以取值时使用特定的注解 使用ItemCat对象接收
     * 返回值结果: SysResult对象
     */
    @PostMapping("/saveItemCat")
    public SysResult saveItemCat(@RequestBody ItemCat itemCat){

        itemCatService.saveItemCat(itemCat);
        return SysResult.success();
    }

    /**
     * 删除商品分类数据
     * URL:  http://localhost:8091/itemCat/deleteItemCat?id=1&level=1
     * 参数: id/level
     * 返回值: SysResult对象
     */
    @DeleteMapping("/deleteItemCat")
    public SysResult deleteItemCat(ItemCat itemCat){

        itemCatService.deleteItemCat(itemCat);
        return SysResult.success();
    }

    /**
     * URL地址:/itemCat/updateItemCat
     * 参数: 修改的form表单 json串
     * put请求
     */
    @PutMapping("/updateItemCat")
    public SysResult updateItemCat(@RequestBody ItemCat itemCat){

        itemCatService.updateItemCat(itemCat);
        return SysResult.success();
    }

}
