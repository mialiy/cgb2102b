package com.jt.controller;

import com.jt.pojo.ItemCatParam;
import com.jt.service.ItemCatParamService;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/itemCatParam")
public class ItemCatParamController {

    @Autowired
    private ItemCatParamService itemCatParamService;

    /**
     * 实现商品分类参数的查询
     * URL: /itemCatParam/findItemCatParamListByType?itemCatId=564&paramType=1
     * 参数: itemCatId,paramType
     * 返回值: SysResult对象
     */
    @GetMapping("/findItemCatParamListByType")
    public SysResult findItemCatParamListByType(ItemCatParam itemCatParam){

        List<ItemCatParam> paramList =
                itemCatParamService.findParamListByType(itemCatParam);
        return SysResult.success(paramList);
    }


    /**
     * 实现商品分类新增操作
     * URL:  /itemCatParam/addItemCatParam
     * 参数: itemCatParam接收   注解格式要求
     * 返回值: SysResult对象
     */
    @PostMapping("/addItemCatParam")
    public SysResult addItemCatParam(@RequestBody ItemCatParam itemCatParam){

        itemCatParamService.addItemCatParam(itemCatParam);
        return SysResult.success();
    }

    /**
     * 实现商品分类参数的更新
     * URL: /itemCatParam/updateItemCatParam
     * 参数: ItemCatParam参数
     * 返回值: SysResult对象
     */
    @PutMapping("/updateItemCatParam")
    public SysResult updateItemCatParam(@RequestBody ItemCatParam itemCatParam){

        itemCatParamService.updateItemCatParam(itemCatParam);
        return SysResult.success();
    }

    /**
     * 商品分类参数删除
     * URL:/itemCatParam/deleteItemCatParamById
     * 参数: paramId=20
     * 返回值: SysResult对象
     */
    @DeleteMapping("/deleteItemCatParamById")
    public SysResult deleteItemCatParam(Integer paramId){

        itemCatParamService.deleteItemCatParam(paramId);
        return SysResult.success();
    }



}
