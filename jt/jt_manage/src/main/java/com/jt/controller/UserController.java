package com.jt.controller;

import com.jt.pojo.User;
import com.jt.service.UserService;
import com.jt.vo.PageResult;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 刘昱江
 * 时间 2021/5/11
 */
@RestController
@CrossOrigin
@RequestMapping("/user")    //抽取公共的请求
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 1.url地址: /user/login
     * 2.请求参数: 用户表单对象的JSON串   post类型
     * 3.返回值结果 SysResult  token?有值 正确        null 错误
     */
    @PostMapping("/login")
    public SysResult login(@RequestBody User user){
        String token = userService.findUserByUP(user);
        if(StringUtils.hasLength(token)){
            return SysResult.success(token);
        }else{
            return SysResult.fail();
        }

        /*try {
            String token = userService.findUserByUP(user);
            if(StringUtils.hasLength(token)){
                return SysResult.success(token);
            }else{
                return SysResult.fail();
            }
        }catch (Exception e){
            e.printStackTrace();
            return SysResult.fail();
        }*/
    }


    /**
     * 需求: 进行分页查询
     * URL地址:  /user/list
     * 请求参数: 使用PageResult对象接收
     * 请求返回值: SysResult对象
     * 请求类型: get请求
     */
    @GetMapping("/list")
    public SysResult findUserByPage(PageResult pageResult){//只有3个参数
        //携带所有的数据返回
        pageResult = userService.findUserByPage(pageResult);
        return SysResult.success(pageResult);
    }

    /**
     * 更新状态信息
     * URL: /user/status/{id}/{status}
     * 参数:  id/status
     * 返回值: SysResult对象
     */
    @PutMapping("/status/{id}/{status}")
    public SysResult updateStatus(User user){

        userService.updateStatus(user);
        return SysResult.success();
    }

    /**
     * 业务分析: 实现用户新增 注意密码加密
     * URL: /user/addUser
     * 请求参数: 用户的form表单
     * 返回值: SysResult对象
     */
     @PostMapping("/addUser")
     public SysResult addUser(@RequestBody User user){

         userService.addUser(user);
         return SysResult.success();
     }

    /**
     * 删除用户
     * URL: /user/{id}
     * 参数: 用户的ID号
     * 返回值: SysResult对象
     * 关于ajax说明:
     *      @RequestBody PUT/POST  要求ajax传递的对象 自己封装为JSON 所以在后端添加注解
     *      GET/DELETE   数据都是普通数据  后端正常接收可以
     *
     */
    @DeleteMapping("/{id}")
    public SysResult deleteUserById(@PathVariable Integer id){

        userService.deleteUserById(id);
        return SysResult.success();
    }
}
