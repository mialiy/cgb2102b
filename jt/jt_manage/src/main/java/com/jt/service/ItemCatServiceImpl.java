package com.jt.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jt.mapper.ItemCatMapper;
import com.jt.pojo.ItemCat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ItemCatServiceImpl implements ItemCatService{

    @Autowired
    private ItemCatMapper itemCatMapper;

    //问题: 代码与数据库交互频繁必须优化
    //思路: 1.只查询一次数据库
    //      2.Map<parentId,List<ItemCat>>
    public Map<Integer,List<ItemCat>> getMap(){
        Map<Integer,List<ItemCat>> map = new HashMap<>();
        //1.查询所有的数据
        List<ItemCat> itemCatList =
                itemCatMapper.selectList(null);
        //2.封装Map集合数据
        //2.1存储依据: 如果key存在?获取子级之后add操作
        //2.2         如果key不存在 则将key存储,同时将自己当做第一个元素保存
        for (ItemCat itemCat : itemCatList){
            int parentId = itemCat.getParentId();
            //判断map集合中是否有父级
            if(map.containsKey(parentId)){
                //有父级 将自己添加到子级中
                map.get(parentId).add(itemCat);
            }else{
                List<ItemCat> list = new ArrayList<>();
                list.add(itemCat);
                map.put(parentId,list);
            }
        }
        return map;
    }

    @Override
    public List<ItemCat> findItemCatList(Integer type) {
        Map<Integer,List<ItemCat>> map = getMap();
        if(type == 1) return map.get(0);
        if(type == 2) return findTwoItemCatList(map);
        return findThreeItemCatList(map);
    }

    //id/status
    @Override
    @Transactional
    public void updateStatus(ItemCat itemCat) {

        itemCatMapper.updateById(itemCat);
    }

    //原子性: 要么同时成功,要么同时失败 事务进行控制
    @Override
    @Transactional  //对方法进行事务的控制
    public void saveItemCat(ItemCat itemCat) {
        //自动填充功能 created/updated
        itemCat.setStatus(true); //启用状态
        itemCatMapper.insert(itemCat);
    }

    /**
     * 实现商品分类删除
     * @param itemCat
     * 实现思路:
     *      根据level 判断层级  之后根据ID删除数据.
     * 删除: 1.手写sql delete from item_cat where id in(100,200...)
     *       2.利用MP机制实现删除
     */
    @Override
    public void deleteItemCat(ItemCat itemCat) {
        //1.判断等级是否为3级
        if(itemCat.getLevel() == 3){
            itemCatMapper.deleteById(itemCat.getId());
        }

        if(itemCat.getLevel() == 2) {
            //1.先删除3级信息
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.eq("parent_id", itemCat.getId());
            itemCatMapper.delete(queryWrapper);
            //2.再删除自己
            itemCatMapper.deleteById(itemCat.getId());
        }

        if(itemCat.getLevel() == 1){
            //1.查询所有的二级菜单
            QueryWrapper<ItemCat> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("parent_id", itemCat.getId());
            List<ItemCat> twoList = itemCatMapper.selectList(queryWrapper);
            for(ItemCat itemCat2 : twoList){
                //parent_id=二级的Id
                QueryWrapper queryWrapper2 = new QueryWrapper();
                queryWrapper2.eq("parent_id", itemCat2.getId());
                itemCatMapper.delete(queryWrapper2);//删除3级
                itemCatMapper.deleteById(itemCat2.getId());//删除2级
            }
            itemCatMapper.deleteById(itemCat.getId());
        }
    }

    @Override
    public void updateItemCat(ItemCat itemCat) {

        itemCatMapper.updateById(itemCat);
    }


    public List<ItemCat> findTwoItemCatList(Map<Integer,List<ItemCat>> map){
        //1.获取一级商品分类
        List<ItemCat> oneList = map.get(0);
        //2.根据一级查询二级
        for(ItemCat itemCat : oneList){
            List<ItemCat> twoList = map.get(itemCat.getId());
            itemCat.setChildren(twoList);
        }
        return oneList;
    }

    private List<ItemCat> findThreeItemCatList(Map<Integer,List<ItemCat>> map) {
        List<ItemCat> oneList = findTwoItemCatList(map);
        for (ItemCat itemCat : oneList){    //1级菜单
            if(itemCat.getChildren() !=null){
                for(ItemCat itemCat2 :itemCat.getChildren()){
                    List<ItemCat> threeList = map.get(itemCat2.getId());
                    itemCat2.setChildren(threeList);
                }
            }
        }
        return oneList;
    }


    //如何实现3级商品分类的嵌套
    //SELECT * FROM item_cat WHERE parent_id=0
    //思考: 1.for循环   2.递归写法   3.尽可能降低数据库查询次数
    //     3.张三表左外连接    mysql执行速度快   业务执行的速度快
    //     单表查询快  关联查询快??
   /* @Override
    public List<ItemCat> findItemCatList(Integer type) {
        *//*if(type == 1){ //只查询一级商品分类
            QueryWrapper<ItemCat> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("parent_id", 0 );
            List<ItemCat> oneList = itemCatMapper.selectList(queryWrapper);
            return oneList;
        }

        if(type==2){
            QueryWrapper<ItemCat> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("parent_id", 0 );
            List<ItemCat> oneList = itemCatMapper.selectList(queryWrapper);
            //根据一级查询二级信息
            for(ItemCat oneItemCat : oneList){
                QueryWrapper<ItemCat> queryWrapper2 = new QueryWrapper<>();
                queryWrapper2.eq("parent_id", oneItemCat.getId());
                List<ItemCat> twoList = itemCatMapper.selectList(queryWrapper2);
                oneItemCat.setChildren(twoList);
            }
            return oneList;
        }*//*


        QueryWrapper<ItemCat> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("parent_id", 0 );
        List<ItemCat> oneList = itemCatMapper.selectList(queryWrapper);
        //根据一级查询二级信息
        if(oneList == null) return null;

        for(ItemCat oneItemCat : oneList){
            QueryWrapper<ItemCat> queryWrapper2 = new QueryWrapper<>();
            queryWrapper2.eq("parent_id", oneItemCat.getId());
            List<ItemCat> twoList = itemCatMapper.selectList(queryWrapper2);
                //根据二级查询3级
                if(twoList == null) return null;

                for(ItemCat twoItemCat : twoList){
                    QueryWrapper<ItemCat> queryWrapper3 = new QueryWrapper<>();
                    queryWrapper3.eq("parent_id", twoItemCat.getId());
                    List<ItemCat> threeList = itemCatMapper.selectList(queryWrapper3);
                    twoItemCat.setChildren(threeList);
                }
            oneItemCat.setChildren(twoList);
        }
            return oneList;
    }*/

     /*@Override
    public List<ItemCat> findItemCatList(Integer type) {
        //获取数据
        Map<Integer,List<ItemCat>> map = getMap();
        return findChildrenList(null,type,map);
    }

    public List<ItemCat> findChildrenList(Integer id,Integer type, Map<Integer,List<ItemCat>> map){
        if(id==null) id = 0;
        List<ItemCat> itemCatList = map.get(id);

        if(type == 1){
            return itemCatList;
        }
        if(itemCatList == null){
            return null;
        }
        for (ItemCat itemCat: itemCatList){
            if(itemCat.getLevel()<type){
                List<ItemCat> childrenList = findChildrenList(itemCat.getId(),type,map);
                itemCat.setChildren(childrenList);
            }
        }
        return itemCatList;
    }*/
}
