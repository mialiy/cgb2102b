package com.jt.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jt.mapper.UserMapper;
import com.jt.pojo.User;
import com.jt.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @author 刘昱江
 * 时间 2021/5/11
 */
@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<User> findAll() {
        return userMapper.selectList(null);
    }

    /**
     * user对象: username/password 明文
     * 业务思路:
     *  1.将密码进行加密处理  md5加密方式
     *  2.根据新的用户名和密码查询数据
     *  3. 结果:null 没查到  u/p错误
     *          不为null u/p正确  返回令牌 token  UUID算法
     *
     *  * @param user
     * @return
     *
     * 数据库信息:   用户名 :admin    密码:admin123456
     */
    @Override
    public String findUserByUP(User user) {
        //1.将密码加密 一般可能添加  盐值: 由公司域名构成
        // hash(md5(www.baidu.com12345))
        String md5Pass =DigestUtils
                        .md5DigestAsHex(user.getPassword().getBytes());
        //2.根据用户名/密码查询数据库.
        user.setPassword(md5Pass);
        //根据对象中不为null的属性当做where条件
        QueryWrapper queryWrapper = new QueryWrapper(user);
        User userDB = userMapper.selectOne(queryWrapper);
        //3.返回密钥token
        String token = UUID.randomUUID().toString()
                       .replace("-", "");
        return userDB==null?null:token;
    }

    /**
     * 业务说明: 将后台数据实现分页查询
     * 分页Sql:
     *      select * from user limit 起始位置,查询记录数
     * 查询第一页 每页20条
     *      select * from user limit 0,20
     * @param pageResult
     * @return
     *
     * MP实现分页查询
     *  MP通过分页对象进行查询,获取所有的分页相关的数据.
     *
     * 参数说明:
     *  page: 定义当前的分页对象 页面/每页的条数
     *  queryWrapper: 条件构造器 只有query属性不为null 才会拼接where条件.
     */

    @Override
    public PageResult findUserByPage(PageResult pageResult) {
        //1.定义分页对象  2个参数
        IPage<User> page = new Page<>(pageResult.getPageNum(),
                              pageResult.getPageSize());
        //2.定义查询条件
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        //3.判断用户是否有参数
        boolean flag = StringUtils.hasLength(pageResult.getQuery());
        queryWrapper.like(flag,"username", pageResult.getQuery());

        //page 参数4个
        page = userMapper.selectPage(page,queryWrapper);
        //4.获取总记录数
        long total = page.getTotal();
        //5.获取分页后的结果
        List<User> userList = page.getRecords();
        return pageResult.setTotal(total).setRows(userList);
    }

    @Override
    public void updateStatus(User user) {   //id/status

        userMapper.updateById(user);
    }

    @Override
    public void addUser(User user) {
        //1.密码加密
        String md5Pass =
                DigestUtils.md5DigestAsHex(user.getPassword().getBytes());
        user.setPassword(md5Pass).setStatus(true);

        userMapper.insert(user);
    }

    @Override
    public void deleteUserById(Integer id) {

        userMapper.deleteById(id);
    }
}
