package com.jt.aop;

import com.jt.pojo.User;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@Aspect
public class SpringAOP {

    private static Map<Integer,User> map = new HashMap();
    /**
     * 需求: 用户第一次查询走目标方法
     *       用户第二次查询走缓存  不执行目标方法
     * 判断依据: 如何判断用户是否为第一次查询?
     *       通过map集合进行判断 有数据 证明不是第一次查询
     * 执行步骤:
     *       1.获取用户查询的参数
     *       2.判断map集合中是否有该数据.
     *       true:  从map集合中get之后返回
     *       false: 执行目标方法,之后将user对象保存到Map中
     */

    //切入点表达式: 拦截service包中的所有方法
    @Around("execution(* com.jt.service..*.*(..))")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        Object result = null;
        //1.获取目标对象的参数
        Object[] args = joinPoint.getArgs();
        //2.强制类型转化为对象
        User user = (User) args[0];
        //3.判断map集合中是否有该数据  user的Id是唯一标识
        int id = user.getId();
        if(map.containsKey(id)){
            //map中有数据
            System.out.println("AOP缓存执行");
            //将获取的数据返回
            return map.get(id);
        }else{
            //map中没有数据 执行目标方法
            result = joinPoint.proceed();
            //将user对象保存到Map中
            map.put(id, user);
            System.out.println("AOP执行目标方法");
        }
        return result;
    }
}
