package com.jt.service;

import com.jt.pojo.User;

public interface UserService {

    //根据User对象查询有效信息
    void findUser(User user);
}
