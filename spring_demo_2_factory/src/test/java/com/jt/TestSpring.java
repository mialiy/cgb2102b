package com.jt;

import com.jt.pojo.User;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Calendar;

public class TestSpring {

    @Test
    public void testStatic(){
        ApplicationContext context =
                        new ClassPathXmlApplicationContext("application.xml");
        Calendar calendar1 = (Calendar) context.getBean("calendar1");
        System.out.println("获取当前时间:"+calendar1.getTime());
    }

    @Test
    public void testInstance(){
        ApplicationContext context =
                new ClassPathXmlApplicationContext("application.xml");
        Calendar calendar1 = (Calendar) context.getBean("calendar2");
        System.out.println("获取当前时间:"+calendar1.getTime());
    }

    @Test
    public void testSpringFactory(){
        ApplicationContext context =
                new ClassPathXmlApplicationContext("application.xml");
        Calendar calendar1 = (Calendar) context.getBean("calendar3");
        System.out.println("获取当前时间:"+calendar1.getTime());
    }

    //测试单例多例/懒加载
    @Test
    public void testUser(){
        ApplicationContext context =
                new ClassPathXmlApplicationContext("application.xml");
        context.getBean("user");
        context.getBean("user");
        context.getBean("user");
    }

    //测试生命周期运行
    @Test
    public void testlife(){
        //1.容器创建
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("application.xml");
        //2.获取对象
        User user = context.getBean(User.class);
        //3.用户调用方法
        user.say();
        //4.只要容器关闭,则对象销毁
        context.close();
    }
}
