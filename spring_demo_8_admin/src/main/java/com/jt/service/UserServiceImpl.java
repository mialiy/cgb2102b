package com.jt.service;

import org.springframework.stereotype.Service;

@Service("target")
public class UserServiceImpl implements UserService{

    @Override
    public void addUser() {
        System.out.println("新增用户");
    }

    @Override
    public void deleteUser() {
        System.out.println("删除用户");
    }
}
