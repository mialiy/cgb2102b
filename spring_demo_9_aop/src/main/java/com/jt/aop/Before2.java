package com.jt.aop;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Aspect
@Order(1)  //控制程序的执行顺序 通过数字进行控制  值越小 越先执行
public class Before2 {

    @Before("@annotation(com.jt.anno.Cache)")
    public void before(){
        System.out.println("我是切面B执行");
    }
}
