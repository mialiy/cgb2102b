package com.jt;

import com.jt.config.SpringConfig;
import com.jt.service.DeptService;
import com.jt.service.DeptServiceImpl;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class TestSpring {

    @Test
    public void test01(){
        ApplicationContext context =
                new AnnotationConfigApplicationContext(SpringConfig.class);
        DeptService deptService = context.getBean(DeptService.class);
        System.out.println(deptService.getClass());
        deptService.updateDept();
    }

    //测试方法的返回值
    @Test
    public void test02(){
        ApplicationContext context =
                new AnnotationConfigApplicationContext(SpringConfig.class);
        DeptService deptService = context.getBean(DeptService.class);
        String result = deptService.after(110);
        System.out.println("test方法接收返回值:"+result);
    }

    @Test
    public void test03(){
        ApplicationContext context =
                new AnnotationConfigApplicationContext(SpringConfig.class);
        DeptService deptService = context.getBean(DeptService.class);
        String result = deptService.after(110);
        System.out.println(result);
    }

    //测试异常通知
    @Test
    public void testThrow(){
        ApplicationContext context =
                new AnnotationConfigApplicationContext(SpringConfig.class);
        DeptService  deptService = context.getBean(DeptService.class);
        deptService.afterThrow();
    }

    //测试环绕通知
    @Test
    public void testAround(){
        ApplicationContext context =
                new AnnotationConfigApplicationContext(SpringConfig.class);
        DeptService  deptService = context.getBean(DeptService.class);
        deptService.doAround();
    }

    @Test
    public void testOrder(){
        ApplicationContext context =
                new AnnotationConfigApplicationContext(SpringConfig.class);
        DeptService  deptService = context.getBean(DeptService.class);
        deptService.doOrder();
    }
}
