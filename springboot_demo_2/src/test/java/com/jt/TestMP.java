package com.jt;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.jt.mapper.DemoUserMapper;
import com.jt.pojo.DemoUser;
import org.apache.catalina.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@SpringBootTest
public class TestMP {

    @Autowired
    private DemoUserMapper userMapper;

    @Test
    public void insert(){
        DemoUser user = new DemoUser();
        user.setName("MP测试").setSex("男").setAge(19);
        userMapper.insert(user);
    }

    //测试更新操作 修改id=231的数据 name="中午吃什么" age=18
    //原则: 根据对象中不为null的属性当做set条件. set name="xxx"
    //      如果ById的操作,则Id必须赋值 并且ID当做唯一where条件
    @Test
    public void updateById(){
        DemoUser user = new DemoUser();
        user.setName("中午吃什么").setAge(18).setId(231);
        userMapper.updateById(user);
    }

    /**
     * 1.查询id=21的用户  根据ID查询数据   1条记录
     * 2.查询name="白骨精" sex=女 的用户   List
     * 知识点:
     *      1.queryWrapper  条件构造器  拼接where条件的.
     *      原则: 根据对象中不为null的属性拼接where条件
     */
    @Test
    public void testSelect(){
        //1.根据ID查询
        DemoUser user = userMapper.selectById(21);
        System.out.println(user);
        //2.根据属性查询
        DemoUser user2 = new DemoUser();
        user2.setName("白骨精").setSex("女");
        QueryWrapper<DemoUser> queryWrapper = new QueryWrapper<>(user2);
        List<DemoUser> userList = userMapper.selectList(queryWrapper);
        System.out.println(userList);
    }

    /**
     * 需求: 查询age>18岁  并且性别为女的用户
     * Sql: select * from demo_user where age > 18 and sex="女"
     * 特殊字符:  > gt   < lt   = eq
     *           >= ge  <= le   <>ne
     * 默认链接符: and
     *
     * */
    @Test
    public void testSelect2(){
        QueryWrapper<DemoUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.gt("age", 18)
                    .eq("sex", "女");
        List<DemoUser> userList = userMapper.selectList(queryWrapper);
        System.out.println(userList);
    }

    /**
     * 练习like关键字
     *      查询name中包含"精"字的数据
     * Sql: like "%精%"
     *      以精开头    like "精%"   likeRight
     *      以精结尾    like "%精"   likeleft
      */
    @Test
    public void testSelect3(){
        QueryWrapper<DemoUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("name", "精");
        List<DemoUser> userList = userMapper.selectList(queryWrapper);
        System.out.println(userList);
    }

    /**
     * 查询sex=男的数据,以id倒序排列
     * Sql: select * from demo_user where sex='男' order by id desc
     */
    @Test
    public void testSelect4(){
        QueryWrapper<DemoUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("sex", "男")
                    .orderByDesc("id");
        List<DemoUser> userList = userMapper.selectList(queryWrapper);
        System.out.println(userList);
    }

    /**
     * 5.查询id= 1,3,5,6,7的用户
     * Sql: select * from demo_user where id in (xxx,xx,xx)
     */
    @Test
    public void testSelect5(){
        //数组使用包装类型
        Integer[] ids = {1,3,5,6,7};
        //需要将数组转化为集合
        List idList = Arrays.asList(ids);
        QueryWrapper<DemoUser> queryWrapper = new QueryWrapper<>();
        //queryWrapper.in("id", idList);    //根据list查询 list集合功能丰富
        queryWrapper.in("id", ids); //数组必须包装类型
        List<DemoUser> userList = userMapper.selectList(queryWrapper);
        System.out.println(userList);
    }

    /**
     * 需求: 如果根据name属性和age属性查询数据. 有时某个数据可能为null,
     *      要求动态查询!!!
     *      where name=xxx age>xxxx
     * 伪Sql: select * from demo_user
     *              where name!=null name=xxx and age!=null age>xxx
     * condition: 内部编辑一个判断的条件
     *                      如果返回值结果为true 则拼接该字段.
     *                      如果为false 则不拼接该字段
     * StringUtils.hasLength(name) 判断字符串是否有效
     */
    @Test
    public void testSelect6(){
        QueryWrapper<DemoUser> queryWrapper = new QueryWrapper<>();
        String name = "";
        int age = 18;
        //boolean flag =  name !=null && !"".equals(name);

        queryWrapper.eq(StringUtils.hasLength(name),"name",name)
                    .gt(age>0, "age",age);
        List<DemoUser> userList = userMapper.selectList(queryWrapper);
        System.out.println(userList);
    }

    /**
     * 需求: 只想查询第一列数据   selectObjs
     * 说明: queryWrapper=null 不需要where条件
     * selectObjs:
     *      1.一般根据条件查询Id的值,查询之后为后续的sql提供数据支持
     *      2. 有时用户只需要查询ID的值,并不需要其他数据项时 使用objs.
    */
    @Test
    public void testSelect7(){
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("sex","男");
        List objs = userMapper.selectObjs(queryWrapper);
        System.out.println(objs);
    }

    /**
     * 需求: 想查询name/sex字段
     *  queryWrapper.select("name","sex"); 挑选执行字段
     */
    @Test
    public void testSelect8(){
        QueryWrapper<DemoUser> queryWrapper = new QueryWrapper();
        queryWrapper.select("name","sex");
        List objs = userMapper.selectList(queryWrapper);
        System.out.println(objs);
    }

    /**
     *   返回有效字段的查询
     */

    @Test
    public void testSelect9(){
        QueryWrapper<DemoUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("name","sex");
        //挑选查询字段返回数据了解objs 和select 用法
        List<Map<String,Object>> lists =
                            userMapper.selectMaps(queryWrapper);
        System.out.println(lists);
    }

    /**
     * 更新数据
     *      将name="中午吃什么" 改为name="晚上吃什么"
     *      性别: 改为 其他
     * Sql:
     *      update demo_user set name="xxx",sex="其他"
     *          where name="xxxx"
     * 参数说明:
     *      1.entity 实体对象  需要修改的数据进行封装
     *      2.updateWrapper 条件构造器
     */
    @Test
    public void testSelect10(){
        DemoUser user = new DemoUser();
        user.setName("晚上吃什么").setSex("其他");
        UpdateWrapper updateWrapper = new UpdateWrapper();
        updateWrapper.eq("name", "中午吃什么");
        userMapper.update(user,updateWrapper);
    }






}

