package com.jt;

import com.jt.mapper.DemoUserMapper;
import com.jt.pojo.DemoUser;
import org.apache.catalina.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class TestSpringBoot {

    @Autowired
    private DemoUserMapper userMapper;

    //数据的查询
    @Test
    public void testFindAll(){
        //代理的测试  jdk动态代理
        System.out.println(userMapper.getClass());
        System.out.println(userMapper.findAll());
    }

    //新增用户
    @Test
    public void testInsert(){
        DemoUser user = new DemoUser();
        user.setId(null).setName("mybatis信息").setAge(18).setSex("男");
        userMapper.insertUser(user);
    }

    //将mybatis name="mybatis信息" 改为"测试信息".要求sex="男"
    //update demo_user set name="测试信息"
    // where name="mybatis信息" and sex="xx"
    //更新操作
    @Test
    public void testUpdate(){
        //1.封装数据
        String oldName = "mybatis信息";
        String nowname = "测试信息";
        String sex = "男";
        userMapper.updateUser(oldName,nowname,sex);
    }

    //测试MybatisPlus
    @Test
    public void testSelect(){

        List<DemoUser> userList =
                userMapper.selectList(null);
        System.out.println(userList);
    }

}
