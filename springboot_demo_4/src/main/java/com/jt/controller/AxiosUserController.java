package com.jt.controller;

import com.jt.pojo.User;
import com.jt.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//使用VUE的方式 实现用户的CURD操作
@RestController
@CrossOrigin    //默认值为*号 允许所有的网站访问
//@CrossOrigin("http://www.jt.com")
public class AxiosUserController {

    @Autowired
    private UserService userService;

    /**
     * Axios 案例
     * 1.查询数据库用户列表数据
     * 2.url: /axiosUser/findAll
     * 3.返回值结果: List<User>
     */
     @GetMapping("/axiosUser/findAll")
     public List<User> findAll(){

         return userService.findAll();
     }

    /**
     * 实现根据更新操作
     * URL地址: http://localhost:8090/axiosUser/updateUser
     * 参数:   user对象的JSON串
     * 返回值: 不要求 void
     */
    @PutMapping("/axiosUser/updateUser")
    public void updateUser(@RequestBody User user){

        userService.updateUser(user);
    }

    /**
     * 业务需求: 删除用户信息
     *  URL: http://localhost:8090/axiosUser/deleteUser?id=1
     * 参数: id=1
     * 返回值: void
     */
    @DeleteMapping("/axiosUser/deleteUser")
    public void deleteUser(Integer id){

        userService.deleteUserById(id);
    }
}
